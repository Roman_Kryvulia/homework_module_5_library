USE [R_Kryvulia_Library]
GO


ALTER trigger [tr_authorIUD_ON_author_log] ON [author]
AFTER INSERT, UPDATE, DELETE
AS
BEGIN
--SET NOCOUNT ON;  set @@rowcount in 0
IF @@ROWCOUNT = 0 RETURN
DECLARE @operation char(1)
DECLARE @ins int = (select count(*) from INSERTED)
DECLARE @del int = (select count(*) from DELETED)
set @operation = 
case 
	when @ins > 0 and @del > 0 then 'U'  
	when @ins = 0 and @del > 0 then 'D'  
	when @ins > 0 and @del = 0 then 'I'  
end 
---- insert
if @operation = 'I'
begin

	insert into [author_log]
            (author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type,
			book_amount_old, issue_amount_old, total_edition_old, book_amount_new, issue_amount_new, total_edition_new)
	Select 
		inserted.author_id, 
		inserted.name,
		inserted.URL,
		NULL,
		NULL,
		NULL,
		@operation,
		NULL,
		NULL,
		NULL,
		inserted.book_amount,
		inserted.issue_amount,
		inserted.total_edition
		FROM [author]
	inner join inserted on [author].author_id = inserted.author_id
end

---- delete
if @operation = 'D'
begin
	insert into [author_log]
	(author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type,
	book_amount_old, issue_amount_old, total_edition_old, book_amount_new, issue_amount_new, total_edition_new)
	Select 
		NULL,
		NULL,
		NULL,
		deleted.author_id, 
		deleted.name,
		deleted.URL,
		@operation,
		deleted.book_amount,
		deleted.issue_amount,
		deleted.total_edition,
		NULL,
		NULL,
		NULL
	FROM deleted 
end
--- update (+update table [author])
if @operation = 'U'
begin
	 UPDATE [author]
     SET updated = GETDATE(),
       updated_by  = system_user
     FROM [inserted]
     WHERE author.author_id = inserted.author_id
 			insert into [author_log]
			(author_id_new, name_new, URL_new, author_id_old, name_old, URL_old, operation_type,
			book_amount_old, issue_amount_old, total_edition_old, book_amount_new, issue_amount_new, total_edition_new)
			select
		    inserted.author_id, 
		    inserted.name,
		    inserted.URL,
			deleted.author_id, 
		    deleted.name,
		    deleted.URL,
		    @operation,
			deleted.book_amount,
		    deleted.issue_amount,
		    deleted.total_edition,
			inserted.book_amount,
		    inserted.issue_amount,
		    inserted.total_edition
						FROM [author]
				 inner join  inserted on [author].author_id = inserted.author_id
				 inner join deleted on [author].author_id = deleted.author_id
				 			 
	end

END
GO